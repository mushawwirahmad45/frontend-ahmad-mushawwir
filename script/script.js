var apiUrl = "https://suitmedia-backend.suitdev.com/api/ideas";

var lengthInfo = document.getElementById("lengthInfo");
var dataDisplayElement = document.getElementById("dataDisplay");
var paginationElement = document.getElementById("pagination");
var perPageSelect = document.getElementById("perPageSelect");
var sortSelect = document.getElementById("sortSelect");

var itemsPerPage = 8;
var ideas = [];

var storedIdeas = localStorage.getItem("ideas");
var ideas = storedIdeas ? JSON.parse(storedIdeas) : [];

// TO SAVE TO LOCAL STORAGE
function saveToLocalStorage() {
  localStorage.setItem("ideas", JSON.stringify(ideas));
}

perPageSelect.addEventListener("change", function () {
  itemsPerPage = parseInt(perPageSelect.value);
  displayPage(1);
});

sortSelect.addEventListener("change", function () {
  displayPage(1);
});

// TO LOAD DATA AND DISPLAY API
function loadData() {
  axios
    .get(apiUrl, {
      params: {
        "page[number]": 1,
        "page[size]": 10,
        "append[]": ["small_image", "medium_image"],
        sort: "-published_at",
      },
      headers: {
        Accept: "application/json",
      },
    })
    .then((response) => {
      ideas = response.data.data;
      var totalItems = ideas.length;
      var totalPages = Math.ceil(totalItems / itemsPerPage);

      if (!storedIdeas) {
        displayPage(1);
      }

      for (var i = 1; i <= totalPages; i++) {
        var li = document.createElement("li");
        li.classList.add("page-item");
        var a = document.createElement("a");
        a.classList.add("page-link");
        a.href = "#";
        a.textContent = i;
        a.addEventListener("click", function (event) {
          event.preventDefault();
          var pageNumber = parseInt(event.target.textContent);
          displayPage(pageNumber);
        });

        li.appendChild(a);
        paginationElement.appendChild(li);
      }
    })
    .catch((error) => console.error("Error:", error));
}
loadData();

// TO SHOW THE PAGE
function displayPage(pageNumber) {
  var startIndex = (pageNumber - 1) * itemsPerPage;
  var endIndex = Math.min(startIndex + itemsPerPage, ideas.length);
  var ideasToDisplay = ideas.slice(startIndex, endIndex);

  if (sortSelect.value === "newest") {
    ideasToDisplay.sort(
      (a, b) => new Date(b.created_at) - new Date(a.created_at)
    );
  } else if (sortSelect.value === "oldest") {
    ideasToDisplay.sort(
      (a, b) => new Date(a.created_at) - new Date(b.created_at)
    );
  }

  saveToLocalStorage();

  renderIdeas(ideasToDisplay, startIndex, endIndex);
  lengthInfo.innerHTML = `Showing ${startIndex + 1}-${endIndex} of ${
    ideas.length
  }`;
}

// TO DISPLAY THE CONTENT (GET API RESPONSE)
function renderIdeas(ideasToDisplay) {
  var html = "";

  ideasToDisplay.forEach((item) => {
    var createdAtDate = new Date(item.created_at);
    var monthNames = [
      "Januari",
      "Februari",
      "Maret",
      "April",
      "Mei",
      "Juni",
      "Juli",
      "Agustus",
      "September",
      "Oktober",
      "November",
      "Desember",
    ];
    var formattedDate = `${createdAtDate.getDate()}, ${
      monthNames[createdAtDate.getMonth()]
    } ${createdAtDate.getFullYear()}`;

    var content = new DOMParser().parseFromString(item.content, "text/html")
      .body.textContent;

    html += `
      <div class="p-2">
        <div class="card custom-card custom-shadow" style="width: 100%;">
          <div class="card-body">
            <p class="card-text text-grey">${formattedDate}</p>
            <h5 class="card-title">${item.title}</h5>
          </div>
        </div>
      </div>`;
  });
  dataDisplayElement.innerHTML = html;
}